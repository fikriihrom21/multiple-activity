package com.example.multipleactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private String nama, email;

    private TextView tnama, temail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Bundle extras = getIntent().getExtras();

        nama = extras.getString("nama");
        email = extras.getString("email");

        tnama = findViewById(R.id.nama);
        temail = findViewById(R.id.email);

        tnama.setText(nama);
        temail.setText(email);
    }
}